CC ?= gcc

NAME = pcat
BIN = bin/$(NAME)

PREFIX ?= $(DESTDIR)/usr
BINDIR = $(PREFIX)/bin

all: $(BIN)

bin/%: src/%.c
	@mkdir -p bin
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf bin

install: all
	install -Dm755 $(BIN) -t $(BINDIR)

uninstall :
	rm -f $(BINDIR)/$(NAME)

.PHONY : all clean install uninstall
